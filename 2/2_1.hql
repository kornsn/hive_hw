-- homework 2 task 1
select uniquecarrier, count(uniquecarrier) as cancelled_flights, concat_ws(', ', collect_set(a.city)) as cities
from hive_test.main m
join hive_test.airports a
on m.origin == a.iata
where year == 2007 and cancelled == 1
group by uniquecarrier
having (cancelled_flights > 1)
order by cancelled_flights desc;
