create temporary function agent as 'ks.hive.ParseAgentUdf';

create temporary table agents as
select city, agent(user_agent) agent
from hive_test_2.bid
limit 1000;

-- devices by cities
create temporary table devices_by_cities as
select city, agent[0] as device, count(agent[0]) as count
from agents
group by city, agent[0];

--
select city
from devices_by_cities as d1
join (select city, device from devices_by_cities as d2 where d1.city == d2.city order by d2.count desc limit 1) as d3
on d1.city == d2.city
limit 20;
