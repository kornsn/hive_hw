SET hive.exec.compress.output=true; 
SET mapred.output.compression.codec = org.apache.hadoop.io.compress.BZip2Codec;

create external table hive_test_2.bid(
  bid string,
  timestamp_ string,
  ipinyou string,
  user_agent string,
  ip string,
  region string,
  city string,
  ad_exchange string,
  domain string,
  url string,
  anonimus_url_id string,
  ad_slot_id string,
  ad_slot_width string,
  ad_slot_height string,
  ad_slot_visibility string,
  ad_slot_format string,
  ad_slot_floor_price string,
  creative_id string,
  bidding_price string,
  advertiser_id string,
  user_tags string
)
row format delimited fields terminated by '\t'
stored as textfile
location "${hiveconf:db_location}/bid/";
