import httpagentparser

__author__ = 'ks'

unknown = {'name': 'Other'}


def parse_agent(line):
    agent = httpagentparser.detect(line)
    return \
        agent.get('dist', unknown)['name'], \
        agent.get('browser', unknown)['name'], \
        agent.get('os', unknown)['name']
