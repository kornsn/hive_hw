from ua_parser.user_agent_parser import Parse
__author__ = 'ks'


def parse_agent(line):
    result = Parse(line)
    return result['device']['family'], result['user_agent']['family'], result['os']['family']

if __name__ == '__main__':
    line = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.104 Safari/537.36'
    print(parse_agent(line))


