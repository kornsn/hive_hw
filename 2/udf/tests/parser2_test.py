from unittest import TestCase

from parser2 import parse_agent

__author__ = 'ks'


class Parser2Test(TestCase):
    def testNormal(self):
        in_ = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.104 Safari/537.36'
        result = parse_agent(in_)
        self.assertEqual(result, ('Other', 'Chrome', 'Mac OS X'))

    def test_bla(self):
        in_ = 'bla-bla-bla'
        result = parse_agent(in_)
        self.assertEqual(result, ('Other', 'Other', 'Other'))

    def test_empty(self):
        in_ = ''
        result = parse_agent(in_)
        self.assertEqual(result, ('Other', 'Other', 'Other'))
