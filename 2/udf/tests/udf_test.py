from unittest import TestCase

from udf import UDF

__author__ = 'ks'


class UDFTest(TestCase):
    def parse(self, line):
        """
        Parser mock
        :param line:
        :return:
        """
        return 'device', 'browser', 'os'

    def setUp(self):
        self.udf = UDF(self.parse)

    def test_normal(self):
        line = 'city\tuser_agent'
        result = self.udf(line)
        self.assertEqual(result, 'city\tdevice\tbrowser\tos')

    def test_bla(self):
        line = ''
        result = self.udf(line)
        self.assertEqual(result, None)

    def test_bla2(self):
        line = '\t'
        result = self.udf(line)
        self.assertEqual(result, '\tdevice\tbrowser\tos')
