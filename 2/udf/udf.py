#!/usr/bin/env python3

from logging import getLogger, basicConfig, WARNING
from sys import stdin

from agent_parser import parse_agent

__author__ = 'ks'

logger = getLogger(__name__)


class UDF(object):
    def __init__(self, parser):
        self.parser = parser

    def __call__(self, line):
        try:
            city, agent_line = line.strip('\r\n').split('\t')
            out = (city,) + self.parser(agent_line)
            return '\t'.join(out)
        except ValueError:
            logger.warning('Cannot process line "%s"', line)
            return None


if __name__ == '__main__':
    basicConfig(level=WARNING, filename='udf.log', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    udf = UDF(parse_agent)
    for line_ in stdin:
        result = udf(line_)
        if result is not None:
            print(result)
