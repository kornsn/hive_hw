-- homewrok 1 task 4
with 
filtered as (
	select origin, dest
	from hive_test.main 
	where month >= 6 
	and month <= 8
	),
iatas as (
	select origin iata
	from filtered
	union all 
	select dest iata
	from filtered
	)
select iata, count(iata) count
from iatas
group by iata
order by count desc
limit 5;
