create external table hive_test.airports (
	iata string,
	airport string,
	city string,
	state string,
	country string,
	lat double,
	long double)
row format SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
stored as textfile
location "${hiveconf:db_location}/airports"
tblproperties("skip.header.line.count"="1");
