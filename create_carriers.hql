create external table hive_test.carriers (code string, description string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
location "${hiveconf:db_location}/carriers"
tblproperties("skip.header.line.count"="1");
