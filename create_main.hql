SET hive.exec.compress.output=true; 
SET mapred.output.compression.codec = org.apache.hadoop.io.compress.BZip2Codec;

create external table hive_test.main (
  Year string,
  Month string,
  DayofMonth string,
  DayOfWeek string,
  DepTime string,
  CRSDepTime string,
  ArrTime string,
  CRSArrTime string,
  UniqueCarrier string,
  FlightNum string,
  TailNum string,
  ActualElapsedTime string,
  CRSElapsedTime string,
  AirTime string,
  ArrDelay string,
  DepDelay string,
  Origin string,
  Dest string,
  Distance string,
  TaxiIn string,
  TaxiOut string,
  Cancelled string,
  CancellationCode string,
  Diverted string,
  CarrierDelay string,
  WeatherDelay string,
  NASDelay string,
  SecurityDelay string,
  LateAircraftDelay string)
row format SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
stored as textfile
location "${hiveconf:db_location}/main/"
tblproperties("skip.header.line.count"="1");
