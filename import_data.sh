#!/usr/bin/env bash

set -e

local="$1"
hdfs="$2"

hdfs dfs -copyFromLocal $local/airports.csv $hdfs/airports/
hdfs dfs -copyFromLocal $local/carriers.csv $hdfs/carriers/
hdfs dfs -copyFromLocal $local/2007.csv.bz2 $hdfs/main/
