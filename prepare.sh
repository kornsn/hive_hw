#!/usr/bin/env bash

set -e

if [ -z $1 ]; then
	echo "Usage: $0 <local_files_location>" 1>&2
	exit 1
fi

db_location="/data/hive/1/input"
local_files_location="$1"

echo "Remove existing data"
hdfs dfs -rm -r -f $db_location

echo "Map data to hive"

hive \
    -v \
    -hiveconf db_location=$db_location \
    -e "$(cat drop_old_database.hql \
            create_database.hql \
            create_airports.hql \
            create_carriers.hql \
            create_main.hql)"

echo "import data"
source import_data.sh $local_files_location $db_location
